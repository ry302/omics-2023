# Omics 2023

Data: 
- Proto
- Meta

Timeline:
- Week12
- Week20
- Week28
- Week36

Training Code:
- Download whole repo
- Open meta_proto_4_weeks_13-08-2023_gitlab.Rmd
- Set File paths in code


Test Code:
- Download whole repo
- Open Pretrained_models folder
- Run model_tester.Rmd
- Set File paths in code

